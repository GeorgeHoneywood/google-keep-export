# Google Keep export

This is a simple python script that helps you to convert the ".html" files provided by Google Takeout to a more machine readable JSON file. It depends on lxml (for parsing the html), which can be installed using pip with:

```pip install lxml```

To run it you will need to place the script in the Takeout folder, above the Keep directory. Simply execute it with something like:

```python3 takeout.py```

This should generate a JSON file (named according to the current time) with this schema for text entries:

```json
{
    "title": "Name of entry",
    "time": "2018-11-26T19:44:27",
    "text": "Contents of the entry, deliminated with \n characters"
}
```

This is the schema for bullet listed notes:

```json
{
    "title": "Name of entry",
    "time": "2018-12-01T23:39:55",
    "uncheckedItems": [
        "First unchecked item",
        "Second unchecked item"
    ],
    "checkedItems": [
        "Checked item 1",
        "Checked item 2"
    ]
}
```

Feel free to submit any pull requests with suggestions or improvements.